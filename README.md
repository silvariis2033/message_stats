# Message stats

## Описание

Проект **Message_stats** представляет собой клиент-серверное приложение для записи и мониторинга сообщений агентов. В проекте используются Django для серверной части и FastAPI для клиентской части.<br> Также предоствлен Agent, который отправляет сообщения на клиентскую часть, которая вдальнейшем отправляет на сервер.
<br>
В админке можно только просматривать записи агентов. Добавление, изменение и удаление записей отключено для модели `Agent`.

## Структура проекта

  **Task_Staffcop/**
   - **agent/**
     - **Agent.py**
     - **.env.example**
     
   - **entrypoint/**
     - **entrypoint.py**
     - **.env.example**
     
   - **practice_task/**
     - **practice_task/**
       - **__init__.py**
       - **settings.py**
       - **urls.py**
       - **wsgi.py**

     - **task_stats/**
       - **__init__.py**
       - **admin.py**
       - **apps.py**
       - **models.py**
       - **tests.py**
       - **views.py**
       - **urls.py**
       - **templates/**
         - **task_stats/**
           - **index.html**
       - **migrations/**
         - **__init__.py**

     -  **manage.py**
     - **init_data.json**
     - **.env.example**
   - **README.md**
   - **.gitignore**
   - **requirements.txt**


## Установка

1. Клонируйте репозиторий:

    ```bash
    git clone <repository_url>
    cd <repository_directory>
    ```

2. Установите зависимости:

    ```bash
    python -m venv .venv
    source .venv/bin/activate
    pip install -r requirements.txt
    ```

3. Создайте копию каждого .env.example и переименуйте в .env. После введите свои переменные окружения.

4. Настройте базу данных и примените миграции:

    ```bash
    cd practice_task
    python manage.py migrate
    ```

5. Создайте суперпользователя:

    ```bash
    python manage.py createsuperuser
    ```

## Запуск

### Сервер

1. Запустите сервер Django:

    ```bash
    cd practice_task
    python manage.py runserver
    ```

### Клиент

1. Запустите клиентское приложение FastAPI на отличном PORT'e/HOST'e от сервера Django:

    ```bash
    cd ../entrypoint
    uvicorn entrypoint:app --reload --port your_port
    ```

### Агент
1. Запустите Агента для отправки сообщений:

    ```bash
    cd ../agent
    python agent.py <agent_name>
    ```
## Логирование

В проекте настроено логирование ошибок. Логи записываются в файлы  с расширением `*.log` 
в директориях `agent` и `entrypoint`. 


