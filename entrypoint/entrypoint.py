from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from dotenv import load_dotenv

from sqlalchemy import create_engine, MetaData
from sqlalchemy.exc import SQLAlchemyError
from sqlalchemy.orm import sessionmaker
import logging
import os

load_dotenv()

logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s - %(levelname)s - %(message)s (%(filename)s)',
                    handlers=[logging.FileHandler('../entrypoint/entrypoint_errors.log'), logging.StreamHandler()])

logger = logging.getLogger(__name__)


host = os.getenv('DB_HOST')
port = os.getenv('DB_PORT')
user = os.getenv('DB_USER')
password = os.getenv('DB_PASSWORD')
dbname = os.getenv('DB_NAME')

engine = create_engine(f'postgresql+psycopg2://{user}:{password}@{host}:{port}/{dbname}')

metadata = MetaData()
metadata.reflect(bind=engine)

agents_table = metadata.tables['task_stats_agent']
message_table = metadata.tables['task_stats_message']

SessionLocal = sessionmaker(autocommit=False, autoflush=False, bind=engine)


app = FastAPI()


class Message(BaseModel):
    agent_name: str
    message_type: str


@app.post("/messages/")
async def receive_message(message: Message):
    try:
        db = SessionLocal()
        agent_query = db.execute(agents_table.select().where(agents_table.c.name == message.agent_name))
        agent = agent_query.fetchone()

        if not agent:
            agent_insert = agents_table.insert().values(name=message.agent_name).returning(agents_table.c.id)
            agent_id = db.execute(agent_insert).fetchone()[0]
            db.commit()
        else:
            agent_id = agent.id

        message_insert = message_table.insert().values(
            message_type=message.message_type,
            agent_id=agent_id,
        )
        db.execute(message_insert)
        db.commit()

        return {"status": "message received"}
    except SQLAlchemyError as e:
        db.rollback()
        logger.error(f"Failed to process message. Error: {e}. HTTP Status=500")
        raise HTTPException(status_code=500, detail=f"Failed to process message. Error: {e}")

    finally:
        db.close()
