from dotenv import load_dotenv
import os

from sqlalchemy import create_engine, MetaData
from sqlalchemy.orm import sessionmaker


class DB_init:
    def __init__(self):
        load_dotenv()
        self.username = os.getenv("DB_USERNAME")
        self.password = os.getenv("DB_PASSWORD")
        self.host = os.getenv("DB_HOST")
        self.port = os.getenv("DB_PORT")
        self.dbname = os.getenv("DB_NAME")
        self.engine = None
        self.metadata = None
        self.Session = None

    def connect_to_db(self):
        self.engine = create_engine(
            f'postgresql+psycopg2://{self.username}:{self.password}@{self.host}:{self.port}/{self.dbname}'
        )
        self.metadata = MetaData()
        self.metadata.reflect(bind=self.engine)
        self.Session = sessionmaker(bind=self.engine, autocommit=False, autoflush=False)
        return self.engine


class AgentService(DB_init):
    def __init__(self):
        super().__init__()
        self.engine = self.connect_to_db()
        self.agents_table = self.metadata.tables['task_stats_agent']
        self.SessionLocal = None

    def get_agent_id(self, name):
        self.SessionLocal = self.Session()
        agent_query = self.SessionLocal.execute(self.agents_table.select().where(self.agents_table.c.name == name))
        agent = agent_query.fetchone()

        if not agent:
            agent_insert = self.insert_agent(name)
            agent_id = self.SessionLocal.execute(agent_insert).fetchone()[0]
            self.SessionLocal.commit()
            self.SessionLocal.close()
            return agent_id

        self.SessionLocal.close()
        return agent.id

    def insert_agent(self, name):
        agent_insert = self.agents_table.insert().values(name=name).returning(self.agents_table.c.id)
        return agent_insert


class MessageService(DB_init):
    def __init__(self):
        super().__init__()
        self.engine = self.connect_to_db()
        self.message_table = self.metadata.tables['task_stats_message']
        self.SessionLocal = None

    def insert_message(self, message, agent_id):
        self.SessionLocal = self.Session()
        message_insert = self.message_table.insert().values(
            message_type=message,
            agent_id=agent_id,
        )
        self.SessionLocal.execute(message_insert)
        self.SessionLocal.commit()
        self.SessionLocal.close()
