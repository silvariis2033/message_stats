import logging
import os
import sys
import requests
import random
from typing import List
from dotenv import load_dotenv


load_dotenv()

logging.basicConfig(level=logging.ERROR,
                    format='%(asctime)s - %(levelname)s - %(message)s (%(filename)s)',
                    handlers=[logging.FileHandler('agent_error.log'), logging.StreamHandler()])

logger = logging.getLogger(__name__)


def send_messages(agent: str) -> None:
    message_types: List[str] = ['password', 'launch', 'message']
    num_messages: int = random.randint(1, 5)

    for i in range(num_messages):
        message: str = random.choice(message_types)
        data: dict = {"agent_name": agent,
                      "message_type": message
                      }
        try:
            response = requests.post(f"http://{host}:{port}/messages/", json=data)
            response.raise_for_status()
            print(f"Sent: {message}")
        except requests.exceptions.RequestException as e:
            logger.error(f"Request Exception: error {e}")


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Should be 'python agent.py <Agent_name>'")
        exit(1)

    agent_name: str = sys.argv[1]
    host = os.getenv("HOST")
    port = os.getenv("PORT")
    send_messages(agent_name)
