from django.http import HttpRequest, HttpResponse
from django.shortcuts import render
from .models import Message


def index(request: HttpRequest) -> HttpResponse:
    message_count: int = Message.objects.count()
    return render(request, 'task_stats/index.html', {'message_count': message_count})
