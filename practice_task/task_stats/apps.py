from django.apps import AppConfig


class TaskStatsConfig(AppConfig):
    name = 'task_stats'
