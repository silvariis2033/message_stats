from typing import Tuple, List

from django.db import models


class Agent(models.Model):
    name: str = models.CharField(max_length=100, unique=True)

    def __str__(self) -> str:
        return self.name


class Message(models.Model):
    message_types: List[Tuple[str, str]] = [
        ('password', 'Password entry'),
        ('launch', 'Launch app'),
        ('message', 'Message send'),
    ]

    message_type: str = models.CharField(max_length=20, choices=message_types)
    agent: str = models.ForeignKey(Agent, on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f'{self.agent}: {self.message_type}'
